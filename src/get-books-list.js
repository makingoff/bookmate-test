var Promise = require('promise');

module.exports = function (url)
{
  return new Promise(function (resolve, reject)
  {
    var require = new XMLHttpRequest();
    require.open('GET', url, true);
    require.onreadystatechange = function ()
    {
      if (require.readyState !== 4) return;
      var response = null;
      if (require.status === 200)  {
        try {
          response = JSON.parse(require.responseText);
        } catch (e) {
          reject(e);
        }
        resolve(response);
      } else {
        reject(require.statusText);
      }
    };
    require.send(null);
  });
};
