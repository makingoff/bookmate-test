var Promise = require('promise');

function getData(response, resolve, reject)
{
  var parserIns = new DOMParser();
  response = response.split('\n');
  response.shift();
  response = response.join('');
  htmlDOM = null;
  try {
    htmlDOM = parserIns.parseFromString(response, 'text/html');
  } catch (e) {
    reject(e);
  }
  resolve(htmlDOM);
}

function getHtmlDomByUrl(url)
{
  return new Promise(function (resolve, reject)
  {
    var require = new XMLHttpRequest();
    require.open('GET', url, true);
    require.onreadystatechange = function ()
    {
      if (require.readyState !== 4) return;
      if (require.status === 200)  {
        getData(require.responseText, resolve, reject);
      } else {
        reject(require.statusText);
      }
    };
    require.send(null);
  });
}

module.exports = getHtmlDomByUrl;
