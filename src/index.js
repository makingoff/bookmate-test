var Promise = require('promise');
var getBooksList = require('./get-books-list');
var getHtmlDomByUrl = require('./get-html-dom-by-url');
var renderBooksList = require('./render-books-list');
var renderReader = require('./render-reader');
var indexOf = Function.prototype.call.bind(Array.prototype.indexOf);

var contain = document.getElementById('contain');
var booksCache = [];

function setStateBooksList()
{
  getBooksList('books.json')
  .then(function (response)
  {
    booksCache = response;
    renderBooksList(contain, response);
  })
  .catch(function (response)
  {
    console.error(response);
  });
}

function setStateReader(uuid)
{
  booksCache.forEach(function (book)
  {
    if (book.book_uuid == uuid) {
      getHtmlDomByUrl(book.text)
      .then(function (htmlDOM)
      {
        renderReader(contain, htmlDOM);
      })
      .catch(function (response)
      {
        console.error(response);
      });
    }
  });
}

setStateBooksList();

contain.addEventListener('click', function (e)
{
  if (~indexOf(e.target.classList, 'js-read')) {
    setStateReader(e.target.getAttribute('data-uuid'));
  }
});

contain.addEventListener('click', function (e)
{
  if (~indexOf(e.target.classList, 'js-back')) {
    setStateBooksList();
  }
});
