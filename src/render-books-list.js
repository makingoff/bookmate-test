var _ = require('lodash');

module.exports = function (contain, books)
{
  var bookWrapTemplate = _.template(document.getElementById('template_book_contain').innerHTML);
  contain.innerHTML = bookWrapTemplate({});
  var bookContain = contain.getElementsByClassName('book')[0];
  var bookTemplate = _.template(document.getElementById('template_book').innerHTML);
  var text = '';
  books.forEach(function (book)
  {
    text += bookTemplate({
      author: book.authors,
      preview: book.cover.url,
      title: book.title,
      tags: book.tags,
      uuid: book.book_uuid
    });
  });
  bookContain.innerHTML = text;
};
