var _ = require('lodash');
var splice = Function.prototype.call.bind(Array.prototype.splice);
var indexOf = Function.prototype.call.bind(Array.prototype.indexOf);

function getElementHeight(element)
{
  var height = 0;
  var styles = window.getComputedStyle(element, '');
  ['height', 'paddingTop', 'paddingBottom'].map(function (param) {
    height += parseInt(styles[param], 10);
  });
  return height;
}

function preparePages(htmlDOM, jsPage, cachePages)
{
  var jsPageHeight = getElementHeight(jsPage);

  var paraghs = Array.prototype.slice.call(htmlDOM.getElementsByTagName('p'), 0);
  var i, paraghsLength = paraghs.length;
  var j, k, word, wordsLength;
  var clonedParagh;
  var clonedParaghText;
  var clonedParaghWords;
  var text;
  var iterationPage = 0;
  for (i = 0; i < paraghsLength; i += 1) {
    if (!cachePages[iterationPage]) {
      cachePages[iterationPage] = [];
    }
    clonedParagh = paraghs[i].cloneNode(true);
    jsPage.appendChild(clonedParagh);

    if (getElementHeight(clonedParagh) + clonedParagh.offsetTop > jsPageHeight) {
      text = '';
      clonedParaghText = clonedParagh.innerHTML;
      clonedParaghWords = clonedParaghText.split(/\s+/img);
      wordsLength = clonedParaghWords.length;

      for (j = 0; j < wordsLength; j += 1) {
        word = clonedParaghWords[j];
        text += ' ' + word;
        clonedParagh.innerHTML = text;
        if (getElementHeight(clonedParagh) + clonedParagh.offsetTop > jsPageHeight) {
          clonedParagh.innerHTML = text.substr(0, text.length - word.length - 1);
          if (!clonedParagh.innerHTML.length) {
            jsPage.removeChild(clonedParagh);
          }
          else {
            paraghs[i - 1].innerHTML = text.substr(0, text.length - word.length - 1);
            cachePages[iterationPage].push(paraghs[i - 1].cloneNode(true));
            clonedParagh = document.createElement('p');
            text = '';
            for (k = j; k < wordsLength; k += 1) {
              word = clonedParaghWords[k];
              text += ' ' + word;
            }
            clonedParagh.innerHTML = text;
            splice(paraghs, i, 0, clonedParagh);
            paraghsLength++;
          }
          i--;
          break;
        }
      }
      jsPage.innerHTML = '';
      iterationPage++;
    }
    else {
      cachePages[iterationPage].push(clonedParagh);
    }
  }
  jsPage.innerHTML = '';
  return iterationPage + 1;
}

function renderPage(pageNumber, jsPage, jsPagings, countPages, cachePages)
{
  var readerPagingItem = _.template(document.getElementById('template_reader_paging_item').innerHTML);
  pageNumber--;
  var i;
  jsPage.innerHTML = '';
  for (i = 0; i < cachePages[pageNumber].length; i+=1) {
    jsPage.appendChild(cachePages[pageNumber][i]);
  }
  var text = '';
  for (i = 1; i <= countPages; i+=1) {
    text += readerPagingItem({
      currentPage: pageNumber,
      page: i
    });
  }
  jsPagings.innerHTML = text;
}

function renderReader(contain, htmlDOM)
{
  var pageNumber = 1;
  var cachePages = [];
  var readerContainTemplate = _.template(document.getElementById('template_reader_contain').innerHTML);
  var i;
  contain.innerHTML = readerContainTemplate({});
  var jsPage = contain.getElementsByClassName('js-page')[0];
  var jsPagings = contain.getElementsByClassName('js-pagings')[0];
  var countPages = preparePages(htmlDOM, jsPage, cachePages);
  renderPage(pageNumber, jsPage, jsPagings, countPages, cachePages);

  jsPagings.addEventListener('click', function (e)
  {
    if(~indexOf(e.target.classList, 'js-paging')) {
      renderPage(+ e.target.getAttribute('data-page'), jsPage, jsPagings, countPages, cachePages);
    }
  });
}

module.exports = renderReader;
